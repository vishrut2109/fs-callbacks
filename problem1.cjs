const path = require("path");
const fs = require("fs");

function createAndDeleteJSONFiles(numberOfFilesCreated) {
  const directoryPath = path.join(__dirname, "./random");

  fs.mkdir(directoryPath, { recursive: true }, (err) => {
    if (err) {
      console.error(new Error(err));
    } else {
      console.log("Directory is made");

      for (let index = 0; index < numberOfFilesCreated; index++) {
        let fileName = `random${index + 1}.json`;

        fs.writeFile(
          path.join(directoryPath, fileName),
          JSON.stringify({ carName: "taigun", brand: "volkswagen" }),
          "utf8",
          function (err) {
            if (err) {
              console.error(new Error(err));
            } else {
              console.log(`File ${fileName} is written`);
              fs.unlink(path.join(directoryPath, fileName), (err) => {
                if (err) {
                  console.error(new Error(err));
                } else {
                  console.log(`File ${fileName} is deleted`);
                }
              });
            }
          }
        );
      }
    }
  });
}

module.exports = createAndDeleteJSONFiles;
