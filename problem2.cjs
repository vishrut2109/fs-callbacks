const path = require("path");
const fs = require("fs");

function convertToUpperCaseLowerCase() {
  let directory = __dirname;

  // 1. Read the given file lipsum.txt
  fs.readFile(path.join(directory, "./lipsum.txt"), "utf-8", (err, data) => {
    if (err) {
      console.error(new Error(err));
    } else {
      // 2. Convert the content to uppercase & write to a new file

      let dataInUpperCase = data.replaceAll("\n\n", "").toUpperCase();
      fs.writeFile(
        path.join(directory, "lipsumUpper.txt"),
        JSON.stringify(dataInUpperCase),
        "utf-8",
        function (err) {
          if (err) {
            console.error(new Error(err));
          } else {
            fs.appendFile(
              path.join(directory, "filenames.txt"),
              "lipsumUpper.txt" + "\n",
              "utf-8",
              function (err) {
                if (err) {
                  console.error(new Error(err));
                } else {
                  // 3. Read the new file and convert it to lower case. Then split the contents into sentences.
                  fs.readFile(
                    path.join(directory, "lipsumUpper.txt"),
                    "utf-8",
                    (err, data1) => {
                      if (err) {
                        console.error(new Error(err));
                      } else {
                        let dataInLowerCase = data1
                          .slice(1, data1.length - 3)
                          .toLowerCase();
                        dataInLowerCase = dataInLowerCase.split(". ");

                        fs.writeFile(
                          path.join(directory, "lipsumLower.txt"),
                          JSON.stringify(dataInLowerCase),
                          "utf-8",
                          function (err) {
                            if (err) {
                              console.error(new Error(err));
                            }
                          }
                        );
                        fs.appendFile(
                          path.join(directory, "filenames.txt"),
                          "lipsumLower.txt" + "\n",
                          "utf-8",
                          function (err) {
                            if (err) {
                              console.error(new Error(err));
                            } else {
                              fs.readFile(
                                path.join(directory, "./lipsumLower.txt"),
                                "utf-8",
                                (err, data2) => {
                                  if (err) {
                                    console.error(new Error(err));
                                  } else {
                                    let dataToBeSorted = data2
                                      .slice(2, data2.length - 2)
                                      .split('","');

                                    let sortedData = dataToBeSorted.sort();

                                    fs.writeFile(
                                      path.join(directory, "lipsumSorted.txt"),
                                      JSON.stringify(sortedData),
                                      "utf-8",
                                      function (err) {
                                        if (err) {
                                          console.error(new Error(err));
                                        } else {
                                          fs.appendFile(
                                            path.join(
                                              directory,
                                              "filenames.txt"
                                            ),
                                            "lipsumSorted.txt" + "\n",
                                            "utf-8",
                                            function (err) {
                                              if (err) {
                                                console.error(new Error(err));
                                              } else {
                                                fs.readFile(
                                                  path.join(
                                                    directory,
                                                    "./filenames.txt"
                                                  ),
                                                  "utf-8",
                                                  (err, data3) => {
                                                    if (err) {
                                                      console.error(
                                                        new Error(err)
                                                      );
                                                    } else {
                                                      let filesArray =
                                                        data3.split("\n");
                                                      filesArray =
                                                        filesArray.slice(
                                                          0,
                                                          filesArray.length - 1
                                                        );

                                                      for (
                                                        let index = 0;
                                                        index <
                                                        filesArray.length;
                                                        index++
                                                      ) {
                                                        console.log(
                                                          filesArray[index]
                                                        );
                                                        fs.unlink(
                                                          path.join(
                                                            directory,
                                                            filesArray[index]
                                                          ),
                                                          (err) => {
                                                            if (err) {
                                                              console.error(
                                                                new Error(err)
                                                              );
                                                            }
                                                          }
                                                        );
                                                      }
                                                    }
                                                  }
                                                );
                                              }
                                            }
                                          );
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    }
                  );
                }
              }
            );
          }
        }
      );
    }
  });
}

module.exports = convertToUpperCaseLowerCase;
